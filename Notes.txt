﻿TODO:
1. abstract Hero class implement IHeroAbilities
	- Name: string
	- Health: int
	- Strength: int
	- Dexterity: int
	- Intelligence: int
	- Level: int
	- Experience: int
	- EffectiveStr: Strength + GearBonus
	- EffectiveDex: Dexterity + GearBonus
	- EffectiveInt: Intelligence + GearBonus
	- HeroClass: HeroClass (Dunno here, interface?)
	- Weapon: Weapon
	- HeadArmor: Armor (effectiveness: int.Parse(bonus*0.8))
	- BodyArmor: Armor (effectiveness: int.Parse(bonus*1))
	- LegsArmor: Armor (effectiveness: int.Parse(bonus*0.6))
	- GearBonus: HeadArmor + BodyArmor + LegsArmor


	
	1.1. IHeroAbilities
		- CheckForLevelup()
		- LevelUp()
		- DealDamage()
		- EquipWeapon()
		- EquipArmor()
		- HeroDetails()
		
2. HeroClass class
	typeName: string (warrior, ranger, mage, etc...) 
	Types:
	-Warrior
		Base stats:
			hp: 150
			str: 10
			dex: 3
			int: 1
		On level Up: consts
			hp: +30
			str: +5
			dex: +2
			int: +1

	-Ranger
		Base stats:
			hp: 120
			str: 2
			dex: 10
			int: 2
		On level Up: consts
			hp: +20
			str: +2
			dex: +5
			int: +1
	-Mage
		Base stats:
			hp: 100
			str: 2
			dex: 3
			int: 10
		On level Up: consts
			hp: +15
			str: +1
			dex: +2
			int: +5

3.	abstract Item class implements IItemDetails
	name: string
	itemLevel: int
	type: string

	Details()

	Types: 
		-Armor
		-Weapon
	
	interface IItemDetails
		- Details()
	

4.  abstract Weapon class implements IWeaponMethods
	baseDmg: int
	weapon classes:
		- Melee
			type : Melee
			baseDmg: 15
			scalingDmg: basedmg + 2 * itemLevel
			dmg: int.Parse(scalingDmg + 1.5 * hero.str)

		- Ranged
			type : Ranged
			baseDmg: 5
			scalingDmg: basedmg + 3 * itemLevel
			dmg: int.Parse(scalingDmg + 2 * hero.dex)

		- Magic
			type : Magic
			baseDmg: 25
			scalingDmg: basedmg + 2 * itemLevel
			dmg: int.Parse(scalingDmg + 3 * hero.int)


5. abstract Armor class implements IArmorMethods
	baseHealthBonus: int
	baseStrengthBonus: int
	baseDexterityBonus: int
	baseIntelligenceBonus: int

	ItemLevel: int

	combinedHpBonus = baseStat + scalingStat
	combinedStrBonus = baseStat + scalingStat
	combinedDexBonus = baseStat + scalingStat
	combinedIntBonus = baseStat + scalingStat


	armor classes:
		- Cloth
			baseHealthBonus: 10
			baseIntelligenceBonus: 3
			baseDexterityBonus: 1

			scalingHealthBonus: baseHealthBonus + 5 * itemLevel
			scalingIntelligenceBonus: baseIntelligenceBonus + 2 * itemLevel
			scalingDexterityBonus: baseDexterityBonus + 1 * itemLevel

		- Leather
			baseHealthBonus: 20			
			baseDexterityBonus: 3
			baseStrenghtBonus: 1

			scalingHealthBonus: baseHealthBonus + 8 * itemLevel
			scalingDexterityBonus: baseDexterityBonus + 2 * itemLevel
			scalingStrengthBonus: baseStrengthBonus + 1 * itemLevel

		- Plate
			baseHealthBonus: 30
			baseStrengthBonus: 3
			baseDexterityBonus: 1

			scalingHealthBonus: baseHealthBonus + 12 * itemLevel
			scalingStrengthBonus: baseStrengthBonus + 2 * itemLevel
			scalingDexterityBonus: baseDexterityBonus + 1 * itemLevel