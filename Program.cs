﻿using RpgMaker.Heroes;
using RpgMaker.Items;
using RpgMaker.Menu;
using RpgMaker.Services;
using RpgMakers.Items;
using System;
using System.Collections.Generic;

namespace RpgMaker
{
    class Program
    {
        public static List<Hero> heroes = new List<Hero>();
        public static List<ItemArmor> armors = new List<ItemArmor>();
        public static List<ItemWeapon> weapons = new List<ItemWeapon>();

        public static void Main(string[] args)
        {

            LoggingService.MainMenuPrompt();

        }

        public static void GenerateSomeCharactersAndItems()
        {
            //Generating some armor pieces and adding to a list. Creating items can be done in CLI.
            ItemArmor coolChestPiece = new ItemArmor(Enums.ArmorType.PLATE, "Breastplate of Giants", 10, Enums.EquipmentSlot.BODY);
            ItemArmor coolHeadPiece = new ItemArmor(Enums.ArmorType.PLATE, "Helmet of Some god you probably heard of some time", 4, Enums.EquipmentSlot.HEAD);
            ItemArmor coolLegPiece = new ItemArmor(Enums.ArmorType.CLOTH, "Leggings of Green man", 9, Enums.EquipmentSlot.LEGS);

            armors.Add(coolHeadPiece);
            armors.Add(coolChestPiece);
            armors.Add(coolLegPiece);

            //Generating some weapons and adding to a list. Creating weapons can be done in CLI.
            ItemWeapon coolSword = new ItemWeapon(Enums.WeaponType.MELEE, "Sword of a Thousand truths", 99, Enums.EquipmentSlot.WEAPON);
            ItemWeapon coolBow = new ItemWeapon(Enums.WeaponType.RANGED, "That Bow From the Sunwell", 10, Enums.EquipmentSlot.WEAPON);
            ItemWeapon coolStaff = new ItemWeapon(Enums.WeaponType.MAGIC, "Staff of Jordan", 3, Enums.EquipmentSlot.WEAPON);

            weapons.Add(coolSword);
            weapons.Add(coolBow);
            weapons.Add(coolStaff);

            //Generating some Heroes and adding to list
            Hero arthas = new HeroWarrior("Arthas Menethil");
            Hero sylvanas = new HeroRanger("Sylvanas Windrunner");
            Hero jaina = new HeroMage("Jaina Proudmore");

            heroes.Add(arthas);
            heroes.Add(sylvanas);
            heroes.Add(jaina);

            arthas.EquipArmor(coolHeadPiece);
            arthas.EquipArmor(coolChestPiece);
            arthas.EquipArmor(coolLegPiece);

            arthas.EquipWeapon(coolSword);
        }

        public static void HeroActivities(Hero hero)
        {
            Console.WriteLine($"Do some activities with {hero.name}? (y/n)");
            string input = Console.ReadLine();
            switch (input)
            {
                case "y":
                    UserInputHandler.HandleActivityMenuInput(hero); // Takes user to options for different activities to do with the hero
                    break;
                case "n":
                    LoggingService.MainMenuPrompt(); // Just goes back to main menu
                    break;
                default:
                    Console.WriteLine("Please select y or n.");
                    HeroActivities(hero);
                    break;
            }
            
          
        }




    }
}
