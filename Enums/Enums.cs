﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Enums
{
    enum HeroType
    {
       WARRIOR,
       RANGER,
       MAGE
    }
    enum WeaponType
    {
        MELEE,
        RANGED,
        MAGIC
    }

    enum Attribute
    {
        HP,
        STR,
        DEX,
        INT
    }

    enum ArmorType
    {
        CLOTH,
        LEATHER,
        PLATE
    }

    enum ItemType
    {
        ARMOR,
        WEAPON
    }

    enum EquipmentSlot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
