﻿using RpgMaker.Enums;
using RpgMaker.Items;
using RpgMaker.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMakers.Items
{
    class ItemWeapon: Item
    {
        

        internal WeaponType WeaponType { get; set; }
        internal EquipmentSlot Weapon { get; set; }
        internal int baseDmg { get; set; }
        internal int itemLevel { get; set; }
        internal int scaledDamage { get; set; }

        internal string typeString;
        

        public ItemWeapon(WeaponType weaponType, string name, int itemLevel, EquipmentSlot equipmentSlot): base(name, itemLevel, ItemType.ARMOR, equipmentSlot)
        {
            this.WeaponType = WeaponType;
            
            
            switch (weaponType)
            {
                case WeaponType.MELEE:
                    this.baseDmg = 15;
                    this.scaledDamage = baseDmg + (2 * itemLevel);
                    this.typeString = "Melee";
                    break;
                case WeaponType.RANGED:
                    this.baseDmg = 5;
                    this.scaledDamage = baseDmg + (3 * itemLevel);
                    this.typeString = "Ranged";

                    break;
                case WeaponType.MAGIC:
                    this.baseDmg = 25;
                    this.scaledDamage = baseDmg + (2 * itemLevel);
                    this.typeString = "Magic";

                    break;
                default:
                    break;
            }
        }

        public override void ItemStats()
        {
            LoggingService.ItemWeaponStatsLogging(this);
        }

    }
}
