﻿using RpgMaker.Enums;
using RpgMaker.Items;
using RpgMaker.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMakers.Items
{
    class ItemArmor: Item
    {
        internal ArmorType ArmorType { get; set; }
        internal string armorTypeString;
        internal int baseHpBonus { get; set; }
        internal int baseStrBonus { get; set; }
        internal int baseDexBonus { get; set; }
        internal int baseIntBonus { get; set; }

        internal int itemLevel { get; set; }
        internal int combinedHpBonus { get; set; }
        internal int combinedStrBonus { get; set; }
        internal int combinedDexBonus { get; set; }
        internal int combinedIntBonus { get; set; }
        public ItemArmor(ArmorType armorType, string name, int itemLevel, EquipmentSlot equipmentSlot): base(name, itemLevel, ItemType.ARMOR, equipmentSlot)
        {
            this.ArmorType = armorType;
            
            switch (armorType)
            {
                case ArmorType.CLOTH:
                    this.armorTypeString = "Cloth";
                    this.baseHpBonus = 10;
                    this.baseIntBonus = 3;
                    this.baseDexBonus = 1;
                    this.combinedHpBonus = baseHpBonus + (5 * itemLevel);
                    this.combinedIntBonus = baseIntBonus + (2 * itemLevel);
                    this.combinedDexBonus = baseDexBonus + (1 * itemLevel);
                    break;

                case ArmorType.LEATHER:
                    this.armorTypeString = "Leather";
                    this.baseHpBonus = 20;
                    this.baseStrBonus = 1;
                    this.baseDexBonus = 3;
                    this.combinedHpBonus = baseHpBonus + (8 * itemLevel);
                    this.combinedStrBonus = baseStrBonus + (1 * itemLevel);
                    this.combinedDexBonus = baseDexBonus + (2 * itemLevel);
                    break;

                case ArmorType.PLATE:
                    this.armorTypeString = "Plate";
                    this.baseHpBonus = 30;
                    this.baseStrBonus = 3;
                    this.baseDexBonus = 1;
                    this.combinedHpBonus = baseHpBonus + (12 * itemLevel);
                    this.combinedStrBonus = baseStrBonus + (2 * itemLevel);
                    this.combinedDexBonus = baseDexBonus + (1 * itemLevel);
                    break;

                default:
                    break;
            }
        }

        public override void ItemStats()
        {
            LoggingService.ItemArmorStatsLogging(this);
        }
    }
}
