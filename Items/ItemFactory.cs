﻿using RpgMaker.Enums;
using RpgMaker.Services;
using RpgMakers.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Items

{
    class ItemFactory
    {
        public Item GenerateItem(string name, int itemLevel, ItemType itemType, EquipmentSlot equipmentSlot)
        {
            switch (itemType)
            {
                case ItemType.ARMOR:
                    ArmorType armorType = UserInputHandler.HandleArmorSelectionInput(); // returns a armor type
                    
                    return new ItemArmor(armorType, name, itemLevel, equipmentSlot);
                   
                case ItemType.WEAPON:
                    WeaponType weaponType = UserInputHandler.HandleWeaponSelectionInput(); // returns a weapon type
                    return new ItemWeapon(weaponType, name, itemLevel, equipmentSlot);
                    
                default:
                    return null;
                    
            }
        }
    }
}
