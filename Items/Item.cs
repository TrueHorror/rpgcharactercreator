﻿using RpgMaker.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Items
{
    abstract class Item
    {
        string name;
        int itemLevel;
        ItemType itemType;
        string equipmentSlotString;
        public Item(string name, int itemLevel, ItemType itemType, EquipmentSlot equipmentSlot)
        {
            this.name = name;
            this.itemLevel = itemLevel;
            this.itemType = itemType;
            this.EquipmentSlot = EquipmentSlot;
            switch (equipmentSlot)
            {
                case EquipmentSlot.HEAD:
                    this.equipmentSlotString = "head";
                    break;
                case EquipmentSlot.BODY:
                    this.equipmentSlotString = "body";
                    break;
                case EquipmentSlot.LEGS:
                    this.equipmentSlotString = "legs";
                    break;
                case EquipmentSlot.WEAPON:
                    break;
                default:
                    break;
            }

        }

        public string Name { get => name;}
        public int ItemLevel { get => itemLevel; }
        public string EquipmentSlotString { get => equipmentSlotString; }
        internal ItemType ItemType { get => itemType; }
        internal EquipmentSlot EquipmentSlot { get; set; }

        public abstract void ItemStats();
    }
}
