﻿using RpgMaker.Enums;
using RpgMaker.Heroes;
using RpgMaker.Items;
using RpgMakers.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Services
{
    class UserInputHandler
    {
        public static void MainMenuInputHandler(string menuOption)
        {
            if (MenuOptionValidation(menuOption)) //Checks if input can be parsed to a number
            {
                switch (menuOption)
                {
                    case "1":
                        CreateHeroPrompt();
                        break;
                    case "2":
                        SelectHeroFromRoster();
                        break;
                    case "3":
                        CreateWeaponPrompt();
                        break;
                    case "4":
                        CreateArmorPrompt();
                        break;
                    case "5":
                        Program.GenerateSomeCharactersAndItems();
                        break;
                    case "6":
                        ItemDetailsPrompt();
                        break;
                    case "7":
                        break;
                    default:
                        LoggingService.MenuOptionErrorLog();
                        LoggingService.MainMenuPrompt();
                        break;
                }
            }
            else
            {
                LoggingService.InvalidInputLogger(menuOption); // Error saying it's not accepted and not a number
                LoggingService.MainMenuPrompt();
            }
            
        }

        private static void ItemDetailsPrompt()
        {
            Console.WriteLine("View Armors or Weapons?");
            Console.WriteLine("1. Armors");
            Console.WriteLine("2. Weapons");

            string itemSelection = Console.ReadLine();

            if (!MenuOptionValidation(itemSelection))
            {
                LoggingService.InvalidInputLogger(itemSelection);
                ItemDetailsPrompt();
            }

            switch (itemSelection)
            {
                case "1":
                    ChosenArmor().ItemStats();
                    break;
                case "2":
                    ChosenWeapon().ItemStats();
                    break;
                default:
                    LoggingService.InvalidInputLogger(itemSelection);
                    ItemDetailsPrompt();
                    break;
            }
            LoggingService.MainMenuPrompt();


        }


        private static void CreateArmorPrompt()
        {
            Console.WriteLine("Name of the armor:");
            string an = Console.ReadLine();

            int al = ItemLevelInput();

            EquipmentSlot slot = HandleSlot();

            ItemFactory itmf = new ItemFactory();

            Item armor = itmf.GenerateItem(an, al, ItemType.ARMOR, slot);
            Program.armors.Add((ItemArmor)armor);
            Console.WriteLine($"{armor.Name} created and added to other armors.");
        }

        private static void CreateWeaponPrompt()
        {
            
            Console.WriteLine("Name of the weapon:");
            string wn = Console.ReadLine();

            int wl = ItemLevelInput();

            

            ItemFactory itmf = new ItemFactory();
           
            Item weapon = itmf.GenerateItem(wn, wl, ItemType.WEAPON, EquipmentSlot.WEAPON);
            Program.weapons.Add((ItemWeapon)weapon);
            Console.WriteLine($"{weapon.Name} created and added to other weapons.");
                 
        }

        public static void SelectHeroFromRoster()
        {
            Console.WriteLine("Select a hero:");
            LoggingService.DisplayHeroes();
            string heroSelection = Console.ReadLine();

            if (!MenuOptionValidation(heroSelection))
            {
                LoggingService.InvalidInputLogger(heroSelection);
                SelectHeroFromRoster();
            }

            if (int.Parse(heroSelection) < 1 || int.Parse(heroSelection) > Program.heroes.Count)
            {
                LoggingService.InvalidInputLogger(heroSelection);
                SelectHeroFromRoster();
            }

            Hero selectedHero = Program.heroes[int.Parse(heroSelection) -1];
            HandleActivityMenuInput(selectedHero);

        }

        private static void CreateHeroPrompt()
        {
            // Method for creating a new hero. Some Console.WriteLine here but thats just because they're so few.
            Console.WriteLine("What is your heros name?");
            string heroName = Console.ReadLine();
            Console.WriteLine($"{heroName}? Niiiiice!");
            LoggingService.NewHeroSelectionMenu();

            string classSelection = Console.ReadLine();



            HeroType classEnum = ClassSelectionHandler(classSelection); // Gets type of hero you wish to create

            HeroFactory hf = new HeroFactory();
            Hero newHero = hf.GenerateHero(heroName, classEnum); // Uses a hero factory to generate a hero

            Console.WriteLine($"{newHero.name}, the {newHero.ClassName} created!");
            Program.heroes.Add(newHero); // adds the hero to list with all other heroes

            Program.HeroActivities(newHero); // Calls method in program that asks user if they want to do stuff with the new hero.

        }


        internal static WeaponType HandleWeaponSelectionInput()
        {
            LoggingService.WeaponTypeSelect(); // Displays types of weapon
            string selection = Console.ReadLine();
            if (!MenuOptionValidation(selection))
            {
                LoggingService.InvalidInputLogger(selection);
                HandleWeaponSelectionInput();
            }

            switch (selection) 
            {
                case "1":
                    return WeaponType.MELEE; // returns to i.e ItemFactory

                case "2":
                    return WeaponType.RANGED;

                case "3":
                    return WeaponType.MAGIC;

                default:
                    LoggingService.InvalidInputLogger(selection);
                    HandleWeaponSelectionInput();
                    return WeaponType.MELEE; //Won't reach this either...


            }
        }

        internal static ArmorType HandleArmorSelectionInput()
        {
            LoggingService.ArmorTypeSelect(); // Displays type of armors
            string selection = Console.ReadLine();
            if (!MenuOptionValidation(selection))
            {
                LoggingService.InvalidInputLogger(selection);
                HandleArmorSelectionInput();
            }

            switch (selection) 
            {
                case "1":
                    return ArmorType.CLOTH; // returns to i.e ItemFactory

                case "2":
                    return ArmorType.LEATHER;

                case "3":
                    return ArmorType.PLATE;

                default:
                    LoggingService.InvalidInputLogger(selection);
                    HandleArmorSelectionInput();
                    return ArmorType.CLOTH; //Won't reach this either...
                    

            }
        }

        internal static void HandleActivityMenuInput(Hero hero)
        {
            LoggingService.HeroActivitiesPrompt(hero); // Displays list of activities
            string activitySelected = Console.ReadLine();

            if (!MenuOptionValidation(activitySelected)) // Checks if input is number
            {
                LoggingService.InvalidInputLogger(activitySelected);
                HandleActivityMenuInput(hero);
            }


            switch (activitySelected)
            {
                case "1":
                    hero.Attack(); // Make an attack
                    HandleActivityMenuInput(hero); // Go back to activity menu
                    break;
                case "2":
                    Item weapon = WeaponEquipHandler(); // Gets selected weapon or created weapon
                    
                    hero.EquipWeapon((ItemWeapon)weapon); // Equips it
                    HandleActivityMenuInput(hero);
                    break;

                case "3":
                    hero.UnEquipWeapon(); // Sets weapon in hero to null
                    HandleActivityMenuInput(hero);
                    break;
                case "4":
                    Item armor = ArmorEquipHandler(); // Gets selected armor or created armor

                    hero.EquipArmor((ItemArmor)armor);
                    HandleActivityMenuInput(hero);
                    break;
                case "5":
                    LoggingService.SelectSlotToUnequipMessage(); // What equipment slot to remove item from
                    hero.UnEquipArmor(ChooseSlot()); // Unequips armor from chosen slot
                    HandleActivityMenuInput(hero);
                    break;
                case "6":
                    hero.HeroDetails(); // Displays details
                    HandleActivityMenuInput(hero);
                    break;
                case "7":
                    hero.GrantExp(ExpInput()); // Grant some exp to level up your hero.
                    HandleActivityMenuInput(hero);
                    break;
                case "8":
                    LoggingService.MainMenuPrompt();
                    break;
                default:
                    break;
            }

            

        }

        private static int ExpInput()
        {
            Console.WriteLine("How much exp to give?");
            string expinput = Console.ReadLine();
            if (!MenuOptionValidation(expinput))
            {
                LoggingService.InvalidInputLogger(expinput);
                ExpInput();
            }

            return int.Parse(expinput);

        }

        private static string ChooseSlot()
        {
            string slot = Console.ReadLine();
            if (!MenuOptionValidation(slot))
            {
                LoggingService.InvalidInputLogger(slot);
                ChooseSlot();
            }

            switch (slot)
            {
                case "1":
                    return "head";
                case "2":
                    return "body";
                case "3":
                    return "legs";
                default:
                    LoggingService.InvalidInputLogger(slot);
                    ChooseSlot();
                    break;
            }
            return "";
        }

        private static Item ArmorEquipHandler()
        {
            LoggingService.CreateNewOrSelectArmor();
            string armorOptionSelect = Console.ReadLine();

            if (!MenuOptionValidation(armorOptionSelect))
            {
                LoggingService.InvalidInputLogger(armorOptionSelect);
                ArmorEquipHandler();
            }


            ItemFactory itmf = new ItemFactory();
            switch (armorOptionSelect)
            {
                case "1":

                    Console.WriteLine("Name your armor:");
                    string an = Console.ReadLine();

                    int al = ItemLevelInput();
                    EquipmentSlot slot = HandleSlot();

                    Item armor = itmf.GenerateItem(an, al, ItemType.ARMOR, slot);

                    Program.armors.Add((ItemArmor)armor);
                    return armor;
                case "2":
                    return ChosenArmor();
                default:
                    LoggingService.InvalidInputLogger(armorOptionSelect);
                    HandleSlot();
                    break;
            }
            return null;
        }

        public static Item ChosenArmor()
        {
            List<ItemArmor> armors = Program.armors;
            if (armors.Count == 0)
            {
                Console.WriteLine("Generate some data? (y/n)");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "y":
                        Program.GenerateSomeCharactersAndItems();
                        ChosenArmor();
                        break;
                        
                    case "n":
                        LoggingService.MainMenuPrompt();
                        break;
                    default:
                        Console.WriteLine("Please select y or n.");
                        ChosenArmor();
                        break;
                      
                }

            }
            else
            {
                LoggingService.DisplayArmors();
                string chosenArmor = Console.ReadLine();
                if (!MenuOptionValidation(chosenArmor))
                {
                    LoggingService.InvalidInputLogger(chosenArmor);
                    return ChosenArmor();
                }

                if (int.Parse(chosenArmor) < 1 || int.Parse(chosenArmor) > Program.armors.Count)
                {
                    LoggingService.InvalidInputLogger(chosenArmor);
                    return ChosenArmor();
                }

                return Program.armors[int.Parse(chosenArmor) - 1];

            }
            return ChosenArmor();
            
         
        }

        private static EquipmentSlot HandleSlot()
        {
            LoggingService.EquipmentSlots();
            string slotSelected = Console.ReadLine();

            if (!MenuOptionValidation(slotSelected))
            {
                LoggingService.InvalidInputLogger(slotSelected);
                HandleSlot();
            }

            switch (slotSelected)
            {
                case "1":
                    return EquipmentSlot.HEAD;
                case "2":
                    return EquipmentSlot.BODY;
                case "3":
                    return EquipmentSlot.LEGS;
                default:
                    LoggingService.InvalidInputLogger(slotSelected);
                    HandleSlot();
                    return EquipmentSlot.HEAD;

                    
            }

        }

        private static Item WeaponEquipHandler()
        {
            LoggingService.CreateNewOrSelectWeapon(); // Some logic in this to ask if user wants to select a weapon or create a weapon
            string weaponOptionSelect = Console.ReadLine();

            if (!MenuOptionValidation(weaponOptionSelect))
            {
                LoggingService.InvalidInputLogger(weaponOptionSelect);
                WeaponEquipHandler();
            }

           
            ItemFactory itmf = new ItemFactory();
            switch (weaponOptionSelect)
            {
                case "1":
                    
                    Console.WriteLine("Name your weapon:");
                    string wn = Console.ReadLine();

                    int wl = ItemLevelInput();
                    Item weapon = itmf.GenerateItem(wn, wl, ItemType.WEAPON, EquipmentSlot.WEAPON);
                    Program.weapons.Add((ItemWeapon)weapon);
                    return weapon;
                case "2":
                    return ChosenWeapon();
            }
            return null;
        }

        public static Item ChosenWeapon()
        {
            List<ItemWeapon> weapons = Program.weapons;
            if (weapons.Count == 0)
            {
                Console.WriteLine("Generate some data? (y/n)");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "y":
                        Program.GenerateSomeCharactersAndItems();
                        ChosenWeapon();
                        break;
                    case "n":
                        LoggingService.MainMenuPrompt();
                        break;
                    default:
                        Console.WriteLine("Please select y or n.");
                        ChosenWeapon();
                        break;
                }

            }
            else
            {
                LoggingService.DisplayWeapons();
                string chosenWeapon = Console.ReadLine();
                if (!MenuOptionValidation(chosenWeapon))
                {
                    LoggingService.InvalidInputLogger(chosenWeapon);
                    ChosenWeapon();
                }

                if (int.Parse(chosenWeapon) < 1 || int.Parse(chosenWeapon) > Program.weapons.Count)
                {
                    LoggingService.InvalidInputLogger(chosenWeapon);
                    ChosenWeapon();
                }

                return Program.weapons[int.Parse(chosenWeapon) - 1];

            }

            return ChosenWeapon();
           
            

            
            
        }

        private static int ItemLevelInput()
        {
            Console.WriteLine("Level of the Item:");
            string level = Console.ReadLine();
            return ValidateItemLevelInput(level);
        }

        private static int ValidateItemLevelInput(string wlevel)
        {
            int output;
            if (!(int.TryParse(wlevel, out output)))
            {
                LoggingService.InvalidInputLogger(wlevel);
                ItemLevelInput();
            }
            return int.Parse(wlevel);
            
        }

        private static bool MenuOptionValidation(string menuOption)
        {
            int selectedOption;
            
            return int.TryParse(menuOption, out selectedOption);
        }

        internal static HeroType ClassSelectionHandler(string classSelection)
        {
            switch (classSelection)
            {
                case "1":
                    return HeroType.WARRIOR;
                    
                case "2":
                    return HeroType.RANGER;

                case "3":
                    return HeroType.MAGE;
                default:
                    LoggingService.InvalidInputLogger(classSelection);
                    CreateHeroPrompt();
                    return HeroType.WARRIOR; // Will never reach this though. But needed a return statement for the method to work.
                    
            }
        }
    }
}
