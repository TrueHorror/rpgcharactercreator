﻿using RpgMaker.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using RpgMaker.Services;
using RpgMaker.Heroes;
using RpgMaker.Enums;
using RpgMakers.Items;

namespace RpgMaker.Services
{
    class LoggingService
    {

        public static void MainMenuPrompt()
        {
            Console.WriteLine("Welcome to RPG character creator. Please select an option: ");
            List<string> menuOptions = new List<string>();
            menuOptions.Add("Create a character.");
            menuOptions.Add("Chose a hero.");
            menuOptions.Add("Create a Weapon.");
            menuOptions.Add("Create an armor piece.");
            menuOptions.Add("Generate some data.");
            menuOptions.Add("Get some item details.");
            menuOptions.Add("Exit program.");


            AppMenu characterMenu = new AppMenu(menuOptions.Count, menuOptions);

            foreach (var option in characterMenu.MenuDictionary)
            {
                Console.WriteLine(option.Key + ". " + option.Value);
            }

            string menuInput = Console.ReadLine();

            UserInputHandler.MainMenuInputHandler(menuInput);
        }

        internal static void WeaponTypeSelect()
        {
            Console.WriteLine("What Kind of Weapon do you want to create?:");
            Console.WriteLine("1. Melee...");
            Console.WriteLine("2. Ranged...");
            Console.WriteLine("3. Magic...");

            
            
        }

        internal static void DisplayHeroes()
        {   
            List<Hero> heroes = Program.heroes;
            if (heroes.Count == 0)
            {
                Console.WriteLine("Generate some data and also some heroes? (y/n)");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "y":
                        Program.GenerateSomeCharactersAndItems();
                        UserInputHandler.SelectHeroFromRoster();
                        break;
                    case "n":
                        MainMenuPrompt();
                        break;
                    default:
                        Console.WriteLine("Please select y or n.");
                        DisplayHeroes();
                        break;
                }

            }
            else
            {
                List<string> menuOptions = new List<string>();

                foreach (var hero in heroes)
                {
                    menuOptions.Add($"{hero.name}, a {hero.ClassName}.");
                }

                AppMenu heroSelectMenu = new AppMenu(menuOptions.Count, menuOptions);

                foreach (var option in heroSelectMenu.MenuDictionary)
                {
                    Console.WriteLine(option.Key + ". " + option.Value);
                }

            }
            
            
        }

   
       

        internal static void HeroActivitiesPrompt(Hero hero)
        {
            Console.WriteLine($"What do you want to do with {hero.name}?:");
            Console.WriteLine("1. Try attacking...");
            Console.WriteLine("2. Equip a weapon...");
            Console.WriteLine("3. Unequip a weapon...");
            Console.WriteLine("4. Equip some armor...");
            Console.WriteLine("5. Unequip armor...");
            Console.WriteLine("6. Display details...");
            Console.WriteLine("7. Grant hero some exprerience...");
            Console.WriteLine("8. Go to main menu...");
            
        }

        internal static void NewHeroSelectionMenu()
        {
            Console.WriteLine("Chose a class for your character:");
            Console.WriteLine("1. Warrior");
            Console.WriteLine("2. Ranger");
            Console.WriteLine("3. Mage");
        }

        internal static void InvalidInputLogger(string menuOption)
        {
            Console.WriteLine($"{menuOption} is not an acceptable input. Try again...");
            MenuOptionErrorLog();
        }


        internal static void MenuOptionErrorLog()
        {
            Console.WriteLine("Input has to be a number.");
        }

        internal static void HeroDetailLog(Hero hero)
        {
            Console.WriteLine("Hero Details: ");
            Console.WriteLine("Hero name: " + hero.name);
            Console.WriteLine("Health: " + hero.health);
            Console.WriteLine("Strength: " + hero.effectiveStr);
            Console.WriteLine("Dexterity: " + hero.effectiveDex);
            Console.WriteLine("Intelligence: " + hero.effectiveInt);
            Console.WriteLine("Level: " + hero.level);
            Console.WriteLine("Experience: " + hero.experience);
            Console.WriteLine("XP needed for next level: " + hero.expToNextLevel);
        }

        internal static void ArmorTypeSelect()
        {
            Console.WriteLine("What Kind of armor type do you want?");
            Console.WriteLine("1. Cloth...");
            Console.WriteLine("2. Leather...");
            Console.WriteLine("3. Plate...");
        }

        internal static void AttackLog(string name, int effectiveDmg)
        {
            Console.WriteLine($"The hero swings with it's {name} and attacks for {effectiveDmg}");
        }

        internal static void CreateNewOrSelectWeapon()
        {
            Console.WriteLine("1. Create weapon");
            Console.WriteLine("2. Chose existing weapon");

        }
        
        internal static void CreateNewOrSelectArmor()
        {
            Console.WriteLine("1. Create armor");
            Console.WriteLine("2. Chose existing armor");
        }

        internal static void DisplayWeapons()
        {
            List<ItemWeapon> weapons = Program.weapons;
            if (weapons.Count == 0)
            {
                Console.WriteLine("Generate some data? (y/n)");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "y":
                        Program.GenerateSomeCharactersAndItems();
                        UserInputHandler.ChosenWeapon();
                        break;
                    case "n":
                        MainMenuPrompt();
                        break;
                    default:
                        Console.WriteLine("Please select y or n.");
                        DisplayWeapons();
                        break;
                }

            }
            else
            {
                List<string> menuOptions = new List<string>();

                foreach (var weapon in weapons)
                {
                    menuOptions.Add($"{weapon.Name}, a level {weapon.ItemLevel} {weapon.typeString}.");
                }

                AppMenu weaponSelectMenu = new AppMenu(menuOptions.Count, menuOptions);

                Console.WriteLine("Choose your Weapon:");
                foreach (var option in weaponSelectMenu.MenuDictionary)
                {
                    Console.WriteLine(option.Key + ". " + option.Value);
                }
            }
            
        }

        internal static void EquipmentSlots()
        {
            Console.WriteLine("In what equipment slot is the armor?");
            Console.WriteLine("1. Headpiece");
            Console.WriteLine("2. Chestpiece");
            Console.WriteLine("3. Legpieces");

        }

        internal static void DisplayArmors()
        {
            List<ItemArmor> armors = Program.armors;
            
            
            List<string> menuOptions = new List<string>();

            foreach (var armor in armors)
            {
                menuOptions.Add($"{armor.Name}, a level {armor.ItemLevel} {armor.armorTypeString} piece for your {armor.EquipmentSlotString}.");
            }

            AppMenu armorSelectMenu = new AppMenu(menuOptions.Count, menuOptions);

            Console.WriteLine("Choose your Armor:");
            foreach (var option in armorSelectMenu.MenuDictionary)
            {
                Console.WriteLine(option.Key + ". " + option.Value);
            }

            
        }

        internal static void SelectSlotToUnequipMessage()
        {
            Console.WriteLine("1. head");
            Console.WriteLine("2. body");
            Console.WriteLine("3. legs");

        }

        internal static void EquipedMessage(string name)
        {
            Console.WriteLine($"{name} equiped!");
        }

        internal static void ItemWeaponStatsLogging(ItemWeapon itemWeapon)
        {
            Console.WriteLine($"Item stats for {itemWeapon.Name}");
            Console.WriteLine($"Weapon type: {itemWeapon.typeString}");
            Console.WriteLine($"Weapon level: {itemWeapon.itemLevel}");
            Console.WriteLine($"Damage: {itemWeapon.scaledDamage}");

        }

        internal static void ItemArmorStatsLogging(ItemArmor itemArmor)
        {
            Console.WriteLine($"Item stats for {itemArmor.Name}");
            Console.WriteLine($"Armor type: {itemArmor.armorTypeString}");
            Console.WriteLine($"Armor Slot: {itemArmor.EquipmentSlotString}");
            Console.WriteLine($"Armor level: {itemArmor.itemLevel}");
            Console.WriteLine($"Bonus HP: {itemArmor.combinedHpBonus}");
            Console.WriteLine($"Bonus Dex: {itemArmor.combinedDexBonus}");
            Console.WriteLine($"Bonus Str: {itemArmor.combinedStrBonus}");
            Console.WriteLine($"Bonus Int: {itemArmor.combinedIntBonus}");




        }

    }
}
