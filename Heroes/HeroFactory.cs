﻿using RpgMaker.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes
{
    class HeroFactory
    {
        public Hero GenerateHero(string name, HeroType heroType)
        {
            
            
            switch (heroType)
            {
                case HeroType.WARRIOR:
                    Hero newWarrHero = new HeroWarrior(name);
                    Program.heroes.Add(newWarrHero);
                    return newWarrHero;
             

                case HeroType.RANGER:
                    Hero newRangHero = new HeroRanger(name);
                    Program.heroes.Add(newRangHero);
                    return newRangHero;


                case HeroType.MAGE:
                    Hero newMagHero = new HeroMage(name);
                    Program.heroes.Add(newMagHero);
                    return newMagHero;


                default:
                    return null;
                 
            }
            
           

        }
    }
}
