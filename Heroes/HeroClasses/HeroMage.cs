﻿using RpgMaker.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes
{
    class HeroMage : Hero
    {
        private const int baseHealth = 100;
        private const int baseStrength = 2;
        private const int baseDexterity = 3;
        private const int baseIntelligence = 10;

        private const int lvlUpHp = 15;
        private const int lvlUpStr = 1;
        private const int lvlUpDex = 2;
        private const int lvlUpInt = 5;

        private const string className = "Mage";

        


        public HeroMage(string name) : base(name, className, baseHealth, baseStrength, baseDexterity, baseIntelligence, lvlUpHp, lvlUpStr, lvlUpDex, lvlUpInt)
        {
           
        }

       
    }
}
