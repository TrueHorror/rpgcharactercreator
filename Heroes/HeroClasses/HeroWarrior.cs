﻿using RpgMaker.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes
{
    class HeroWarrior : Hero
    {
        
        private const int baseHealth = 150;
        private const int baseStrength = 10;
        private const int baseDexterity = 3;
        private const int baseIntelligence = 1;

        private const int lvlUpHp = 30;
        private const int lvlUpStr = 5;
        private const int lvlUpDex = 2;
        private const int lvlUpInt = 1;

        private const string className = "Warrior";

        public HeroWarrior(string name) : base(name, className, baseHealth, baseStrength, baseDexterity, baseIntelligence, lvlUpHp, lvlUpStr, lvlUpDex, lvlUpInt)  
        {
        }
    }
}
