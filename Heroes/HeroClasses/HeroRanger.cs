﻿using RpgMaker.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes

{
    class HeroRanger : Hero
    {

        private const int baseHealth = 120;
        private const int baseStrength = 2;
        private const int baseDexterity = 10;
        private const int baseIntelligence = 1;

        private const int lvlUpHp = 20;
        private const int lvlUpStr = 2;
        private const int lvlUpDex = 5;
        private const int lvlUpInt = 1;

        private const string className = "Ranger";


        public HeroRanger(string name): base(name, className, baseHealth, baseStrength, baseDexterity, baseIntelligence, lvlUpHp, lvlUpStr, lvlUpDex, lvlUpInt)
        {
            
            
        }

    }
}
