﻿using RpgMaker.Enums;
using RpgMaker.Items;
using RpgMaker.Services;
using RpgMakers.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes
{
    abstract class Hero : IHeroAbilities 
    {
        public string name { get; set; }
        private string className;
        public string ClassName { get => className; }
        public int health { get; set; }

        private int baseHealth;
        private int baseStrength;
        private int baseDexterity;
        private int baseIntelligence; 

        private int lvlUpHp;
        private int lvlUpStr;
        private int lvlUpDex;
        private int lvlUpInt;

        public int level { get; set; }
        public int experience;
        public int expToNextLevel { get; set; }

        public int effectiveStr { get; set; }
        public int effectiveDex { get; set; }
        public int effectiveInt { get; set; }

        public ItemWeapon Weapon { get; set; }

        public ItemArmor HeadArmor { get; set; }
        public ItemArmor BodyArmor { get; set; }
        public ItemArmor LegArmor { get; set; }

        private int strBonus = 0;
        private int dexBonus = 0;
        private int intBonus = 0;
        private int hpBonus = 0;

        private int effectiveDmg;

        public Hero(string name, string className, int baseHp, int baseStr, int baseDex, int baseInt, int lvlUpHp, int lvlUpStr, int lvlUpDex, int lvlUpInt)
        {
            this.name = name;
            this.className = className;
            this.baseHealth = baseHp;
            this.baseStrength = baseStr;
            this.baseDexterity = baseDex;
            this.baseIntelligence = baseInt;

            this.lvlUpHp = lvlUpHp;
            this.lvlUpStr = lvlUpStr;
            this.lvlUpDex = lvlUpDex;
            this.lvlUpInt = lvlUpInt;

            this.Weapon = null;
            this.HeadArmor = null;
            this.BodyArmor = null;
            this.LegArmor = null;

            this.level = 1;
            this.experience = 0;
            this.expToNextLevel = 100;

            CalculateEffectiveStats();
        }

        private void CalculateGearBonus()
        {
            if (HeadArmor != null)
            {
                this.strBonus = this.strBonus + (int)(HeadArmor.combinedStrBonus * 0.8);
                this.dexBonus = this.dexBonus + (int)(HeadArmor.combinedDexBonus * 0.8);
                this.intBonus = this.intBonus + (int)(HeadArmor.combinedIntBonus * 0.8) ;
                this.hpBonus = this.hpBonus + (int)(HeadArmor.combinedHpBonus * 0.8);
            }

            if (BodyArmor != null)
            {
                this.strBonus = this.strBonus + (int)(BodyArmor.combinedStrBonus * 1);
                this.dexBonus = this.dexBonus + (int)(BodyArmor.combinedDexBonus * 1);
                this.intBonus = this.intBonus + (int)(BodyArmor.combinedIntBonus * 1);
                this.hpBonus = this.hpBonus + (int)(BodyArmor.combinedHpBonus * 1);
            }

            if (LegArmor != null)
            {
                this.strBonus = this.strBonus + (int)(LegArmor.combinedStrBonus * 0.6);
                this.dexBonus = this.dexBonus + (int)(LegArmor.combinedDexBonus * 0.6);
                this.intBonus = this.intBonus + (int)(LegArmor.combinedIntBonus * 0.6);
                this.hpBonus = this.hpBonus + (int)(LegArmor.combinedHpBonus * 0.6);
            }
            CalculateEffectiveStats();



        }

        private void CalculateEffectiveStats()
        {
            this.effectiveStr = baseStrength + strBonus;
            this.effectiveDex = baseDexterity + dexBonus;
            this.effectiveInt = baseIntelligence + intBonus;
            this.health = baseHealth + hpBonus;
        }

        public void CheckForLevelup()
        {
            if (this.experience >= this.expToNextLevel)
            {
                LevelUp();
                float nextExpThreshold = this.expToNextLevel * 0.1f;
                this.expToNextLevel += (int)nextExpThreshold;
                CheckForLevelup();
            }
            

        }

        public void Attack() 
        {

            if (this.Weapon == null)
            {
                LoggingService.AttackLog("fists", 0);
            }
            else
            {
                switch (this.Weapon.WeaponType)
                {
                    case WeaponType.MELEE:
                        DealDamage(this.effectiveStr, 1.5f);
                        break;

                    case WeaponType.RANGED:
                        DealDamage(this.effectiveDex, 2);
                        break;

                    case WeaponType.MAGIC:
                        DealDamage(this.effectiveInt, 3);
                        break;

                    default:
                        break;
                }
            }
           
            

        }

        private void DealDamage(float stat, float mod)
        {
            effectiveDmg = (int)(Weapon.scaledDamage + mod * stat);
            LoggingService.AttackLog(Weapon.Name, effectiveDmg);
        }

        public void EquipArmor(ItemArmor item)
        {
            switch (item.EquipmentSlot)
            {
                case EquipmentSlot.HEAD:
                    this.HeadArmor = item;
                    break;
                case EquipmentSlot.BODY:
                    this.BodyArmor = item;
                    break;
                case EquipmentSlot.LEGS:
                    this.LegArmor = item;
                    break;
                default:
                    break;
            }
            LoggingService.EquipedMessage(item.Name);
            CalculateGearBonus();
            
        }


        public void EquipWeapon(ItemWeapon item)
        {
            this.Weapon = item;
            LoggingService.EquipedMessage(item.Name);


        }


        public void HeroDetails()
        {
            LoggingService.HeroDetailLog(this);
            
        }


        public void LevelUp()
        {
            Console.WriteLine("DING!!!!!!!");
            this.level++;
            this.baseStrength += this.lvlUpStr;
            this.baseHealth += this.lvlUpHp;
            this.baseDexterity += this.lvlUpDex;
            this.baseIntelligence += this.lvlUpInt;
            CalculateEffectiveStats();
        }

        public void GrantExp(int exp)
        {
            this.experience += exp;
            CheckForLevelup();
        }

        public void UnEquipArmor(string armorSlot)
        {
            switch (armorSlot)
            {
                case "body":
                    this.BodyArmor = null;
                    break;
                case "head":
                    this.HeadArmor = null;
                    break;
                case "legs":
                    this.LegArmor = null;
                    break;
                default:
                    break;
            }
            
            this.strBonus = 0;
            this.hpBonus = 0;
            this.dexBonus = 0;
            this.intBonus = 0;
            
            CalculateGearBonus();
            
        }

        public void UnEquipWeapon()
        {
            this.Weapon = null;
        }
    }
}
