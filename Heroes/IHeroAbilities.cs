﻿using RpgMakers.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RpgMaker.Heroes
{
    interface IHeroAbilities
    {
		void GrantExp(int exp);
		void CheckForLevelup();
		void LevelUp();
		void Attack();
		void EquipWeapon(ItemWeapon item);
		void EquipArmor(ItemArmor item);
		void HeroDetails();

		void UnEquipArmor(string armorSlot);

		void UnEquipWeapon();


	}
}
