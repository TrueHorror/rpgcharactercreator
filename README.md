# RPG character creator #
This is an atempt to make a RPG character creator. This was made as a Console Application.

## Functionality ##
**In this application you can:**
-Create characters with a name and classes are: Warrior, Ranger and Mage.
-Create Weapons and Armors to equip your characters with.

## Start demonstration ##
To try out the functionallity, start the programm and follow the instructions. 
You will be presented with a menu asking what you want to do. 
1. Create a Character
2. Chose existing Hero
3. Create a Weapon
4. Create an armor piece
5. generate some data (Heroes, weapons and armor)
6. Get details of items
7. Exit program.

